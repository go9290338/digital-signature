package main

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"errors"
	"fmt"
	"os"
)

type Client struct {
	PrivateKey *rsa.PrivateKey
	PublicKeys *rsa.PublicKey
}

var (
	message = "To Be or not To Be"
)

func main() {
	client := Client{}

	// - LOAD PRIVATE KEY
	var e error
	if client.PrivateKey, e = LoadPrivateKey("private"); e != nil {
		fmt.Println(e)
		return
	}

	// - LOAD PUBLIC KEY
	if client.PublicKeys, e = LoadPublicKey("private"); e != nil {
		fmt.Println(e)
		return
	}

	// - CREATE DIGITAL SIGNATURE
	signature, e := CreateDigitalSignature(message, client.PrivateKey)
	fmt.Printf("Signature : %s\n\n", signature)

	// - VERIFY SIGNATURE
	//signature = "DxufuMZX+/cQ9+qFu+eFRifFk68jtuKOJodBlefG8+iXuOy6mlwI3qrBlOJhPu9rl1ADFaCM0A3wjv0UMzJMRQD8HmVYmtaMXQNsx3oGAwbfFyP1hojMMWaYnnPfU1TFMLKyoDPLsI6GCFtbNNd4ikLfbT2EVkYKq0y9rdXnJ6oFTyhCpUmqo+yPwrc1uR4NcPG314k6bwkMPgcMrfVk4Scp+ePwBls0hWqExQAYi4iE6GMWQyRv0H4kL13jal2AFj6M4KhZHI8l6vkiVzCZgAOn966JvGOn4ezU0vH+POAoQHX21840mdD7J6UJJmfm8aQJ/yprRUWGuwC+AJruHJbGwwzcaNwQTuo4JTXYdgXTH4RjihJUFlhynFzgBiCdtL9hZCqpHLx4lzmigPkqnzDhQ2vSfIeCNvO2IArBtzFHaRZoo2S6QmIEsuaYcqJSzaEyvgwFpkjQu2sr0BBu3U4/3B9HPrDKa/WVlMK/fNj5x23lfvmos4XXFgAX1z8Z0z5KOqDThgDMcyXGYteLBu/uoJo+ZrtIPukBS1z4Euf14wuKz0M2uYxZQzu4dn0vEeRt48VQ/kf/JhuE34JzUxFRB6ovG3cciy2DE/a3DCJQK4wm5hhEScsiMRVttrZzofRZT6XwpkvcjBGWkDmG2xCgzM/nIMx98GlZ09IEWhU="
	verify, e := VerifySignature(signature, message, client.PublicKeys)
	if e != nil {
		fmt.Println(e)
		return
	}
	fmt.Printf("Verify : %v\n\n", verify)

	// - ENCRYPT DATA
	encrypt, e := Encrypt(message, client.PublicKeys)
	if e != nil {
		fmt.Println(e)
		return
	}
	fmt.Printf("Encrypt : %v\n\n", encrypt)

	decrypt, e := Decrypt(encrypt, client.PrivateKey)
	if e != nil {
		fmt.Println(e)
		return
	}
	fmt.Printf("Decrypt : %v", decrypt)
}

func LoadPrivateKey(filepath string) (*rsa.PrivateKey, error) {
	privateData, err := os.ReadFile(filepath)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("Error Load Private Key : %s\n", err))
	}
	privateBlock, _ := pem.Decode(privateData)

	return x509.ParsePKCS1PrivateKey(privateBlock.Bytes)
}

func LoadPublicKey(filepath string) (*rsa.PublicKey, error) {
	publicData, err := os.ReadFile("public")
	if err != nil {

		return nil, errors.New(fmt.Sprintf("Error Load Public Key : %s\n", err))
	}
	publicBlock, _ := pem.Decode(publicData)
	publicDer, err := x509.ParsePKIXPublicKey(publicBlock.Bytes)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("Error Parse Public Key : %s\n", err))
	}
	return publicDer.(*rsa.PublicKey), nil
}

func CreateDigitalSignature(message string, privateKey *rsa.PrivateKey) (string, error) {
	hash := sha256.New()
	if _, e := hash.Write([]byte(message)); e != nil {
		return "", errors.New(fmt.Sprintf("Error hashing message: %v", e))
	}

	hashMessage := hash.Sum(nil)
	signature, e := rsa.SignPKCS1v15(rand.Reader, privateKey, crypto.SHA256, hashMessage)
	if e != nil {
		return "", errors.New(fmt.Sprintf("Error signing message: %v", e))

	}
	return base64.StdEncoding.EncodeToString(signature), nil
}

func VerifySignature(signature, message string, publicKey *rsa.PublicKey) (bool, error) {
	hashSignature, _ := base64.StdEncoding.DecodeString(signature)
	hash := sha256.New()
	if _, e := hash.Write([]byte(message)); e != nil {
		return false, errors.New(fmt.Sprintf("Error hashing message: %v", e))
	}
	hashedMessage := hash.Sum(nil)
	e := rsa.VerifyPKCS1v15(publicKey, crypto.SHA256, hashedMessage, hashSignature)
	if e != nil {
		return false, errors.New(fmt.Sprintf("Error verifying signature: %v", e))
	}
	return true, nil
}

func Encrypt(text string, publicKey *rsa.PublicKey) (string, error) {
	encrypt, e := rsa.EncryptPKCS1v15(rand.Reader, publicKey, []byte(text))
	if e != nil {
		return "", errors.New(fmt.Sprintf("Error Encrypt: %v", e))
	}
	return base64.StdEncoding.EncodeToString(encrypt), nil
}

func Decrypt(ciphertext string, privateKey *rsa.PrivateKey) (string, error) {
	hash, _ := base64.StdEncoding.DecodeString(ciphertext)
	decrypt, e := rsa.DecryptPKCS1v15(rand.Reader, privateKey, hash)
	if e != nil {
		return "", errors.New(fmt.Sprintf("Error Encrypt: %v", e))
	}
	return base64.StdEncoding.EncodeToString(decrypt), nil
}
